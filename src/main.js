// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue';
import App from './App';
import router from './router';

import LoginDialog from './components/LoginDialog';
import ServerConnection from './utils/ServerConnection';
import api from './utils/api';
import esConfig from '../es_config';

import moment from 'moment';

Vue.config.productionTip = false;

/* eslint-disable no-new */

function includeJs(jsFilePath) {
  var js = document.createElement("script");

  js.type = "text/javascript";
  js.src = jsFilePath;

  document.body.appendChild(js);
}

function includeCss(jsFilePath) {
  var js = document.createElement("style");

  js.type = "text/css";
  js.src = jsFilePath;

  document.body.appendChild(js);
}

new Vue({
  el: '#app',
  router,
  components: { App, LoginDialog },
  template: '<App/>',
  methods: {
    async login(login, password, initialization) {
      if(!initialization) await this.initConnection(true);

      const res = await this.getApi().apiCallStub('basic', 'login', {login, password: md5(password)});
      if(res.result === 'ok') {
        localStorage.setItem('attistatSystemsToken', res.token);
        this.connection.user = res.user;
        this.gotoFirstPage();
        this.onLoggedIn();
        return true;
      } 

      return false;
    },

    async logout() {
      await this.getApi().apiCallStub('basic', 'logout', {});

      localStorage.removeItem('attistatSystemsToken');
      this.connection.disconnect();
      delete this.connection;

      console.log('logout slw');
      this.showLoginWindow();

      this.onLoggedOut();
    },

    async initConnection(loginPending) {
      if(this.connection) {
        try{
          this.connection.disconnect();
        } catch(err) {}

        delete this.connection;
      }

      this.connection = new ServerConnection();
      const self = this;
  
      this.connection.onClose = () => {
        console.log('onclose slw');
        this.showLoginWindow();
      }
  
      this.connection.onServerError = (error) => {
        if(error.error == "Не авторизованный доступ") {
          console.log('not auth slw');
          this.showLoginWindow();
        }
        else {
          this.$alert(error.error, 'Ошибка', {
            confirmButtonText: 'OK',
            callback: action => {
            }
          });
  
          $( "#userMessage" ).dialog( "open" ).find("#userMessageText").html(error.error);
        }
      }
        
      try {
        await this.connection.connect(esConfig.ws.proto + '://' + esConfig.ws.host + ':' + esConfig.ws.port + esConfig.ws.path);
      } catch(err) {
        this.$alert(`Не удалось соединиться с сервером ${err || ''}`, 'Error', {
          confirmButtonText: 'OK',
          callback: action => {
            console.log('impossible slw');
            this.showLoginWindow();
          }
        });

        return;
      }

      if(!loginPending) {
        const token = localStorage.getItem('attistatSystemsToken');
        if(token) {
          const res = await this.getApi().apiCallStub('basic', 'whoAmI', {token});
          if(res.user) {
            this.connection.user = res.user;
            this.gotoFirstPage();
            this.onLoggedIn();
            return;
          } else {
            console.log('whoamu slw');
            this.showLoginWindow();
          }
        } else {
          console.log('notoken slw');
          this.showLoginWindow();
        }
      }
    },

    onLoggedIn() {
      if(this.loggedIn) this.loggedIn();

      this.$root.vehicleTree.loadData();
    },

    onLoggedOut() {
      if(this.loggedOut) this.loggedOut();
    },

    showLoginWindow() {
      console.log('show login');
      this.$router.push({ path: 'empty'});

      const vueContainer = document.createElement('div');
      this.$el.appendChild(vueContainer);
      const vm = (new (Vue.extend(LoginDialog))());
      vm.$mount(vueContainer);
      vm.loginCallback = this.login;
    },

    gotoFirstPage() {
      this.$router.push({ path: 'empty'});
    },

    getApi() {
      return new api(this.connection);
    },

    getConnection() {
      return this.connection;
    }
  },
  mounted() {
    includeJs('static/md5.js');

    this.initConnection();
  },
  unmounted() {
    this.connection.close();
  },
  provide() {
    return {
      logout: this.logout,
      getApi: this.getApi,
    }
  }
});
