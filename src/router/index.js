import Vue from 'vue';
import Router from 'vue-router';
import LoginDialog from '@/components/LoginDialog';

import Empty from '@/components/Empty';

import VehicleCard from '@/components/VehicleCard';
import TerminalCard from '@/components/TerminalCard';
import ChannelCard from '@/components/ChannelCard';
import VehicleView from '@/components/VehicleView';

Vue.use(Router);

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/empty',
      name: 'Empty',
      component: Empty,
    },
    {
      path: '/vehicle',
      name: 'VehicleCard',
      component: VehicleCard,
      meta: {reuse:false}
    },
    {
      path: '/terminal',
      name: 'TerminalCard',
      component: TerminalCard,
      meta: {reuse:false}
    },
    {
      path: '/channel',
      name: 'ChannelCard',
      component: ChannelCard,
      meta: {reuse:false}
    },
    {
      path: '/vehicle_view',
      name: 'VehicleView',
      component: VehicleView,
      meta: {reuse:false}
    },
  ],
});
